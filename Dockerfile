FROM node:14.17.4-alpine AS build

WORKDIR /usr/src/app
COPY package*.json ./
RUN npm install
COPY . .
RUN npm run build


# the actual built container only needs the prod dependencies and the dist
FROM node:14.17.4-alpine AS run

ARG NODE_ENV=development
ENV NODE_ENV=${NODE_ENV}

WORKDIR /usr/src/app
COPY package*.json ./
RUN npm install --only=production
COPY --from=build /usr/src/app/dist ./dist
# the app runs on port 3000
EXPOSE 3000
CMD ["node", "dist/main"]
