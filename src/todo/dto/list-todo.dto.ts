import { ApiProperty } from '@nestjs/swagger';
import { IsEnum } from 'class-validator';
import { Status } from '../entities/status.entity';

export class ListTodoDto {
  @ApiProperty()
  @IsEnum(Status)
  status: Status;
}
