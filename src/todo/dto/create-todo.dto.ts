import { ApiBody, ApiProperty } from '@nestjs/swagger';
import { IsEnum, IsNotEmpty, IsOptional } from 'class-validator';
import { Status } from '../entities/status.entity';

export class CreateTodoDto {

  @ApiProperty()
  @IsOptional()
  id: number;

  @ApiProperty()
  @IsNotEmpty()
  text: string;

  @ApiProperty()
  @IsEnum(Status)
  status: Status;
}
