import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Query,
  HttpException,
  HttpStatus,
} from '@nestjs/common';
import { Status } from './entities/status.entity'
import { TodoService } from './todo.service';
import { CreateTodoDto } from './dto/create-todo.dto';
import { UpdateTodoDto } from './dto/update-todo.dto';
import { ListTodoDto } from './dto/list-todo.dto';
import { ApiBody, ApiOkResponse, ApiQuery, ApiResponse, getSchemaPath } from '@nestjs/swagger';
import { Todo } from './entities/todo.entity';

@Controller('todo')
export class TodoController {
  constructor(private readonly todoService: TodoService) {}

  @Post()
  @ApiBody({ type: CreateTodoDto})
  @ApiResponse({status: 201, description: 'The todo was created', type: CreateTodoDto})
  async create(@Body() createTodoDto: CreateTodoDto) {
    return await this.todoService.create(createTodoDto);
  }

  @Get()
  @ApiQuery({ name: 'status', enum: Status})
  // @ApiResponse({status: 200, description: 'The matching todos were returned'})
  @ApiOkResponse({
    description: "The matching todos were returned",
    schema: {
      type: 'array',
      items: {$ref: getSchemaPath(CreateTodoDto)}
    }
  })
  async findAll(@Query() query: ListTodoDto) {
    return await this.todoService.findAll(query);
  }

  @Get(':id')
  @ApiResponse({status: 200, description: 'The matching todo was returned', type: CreateTodoDto})
  @ApiResponse({status: 404, description: 'There is no matching todo'})
  async findOne(@Param('id') id: string) {
    let resp = await this.todoService.findOne(+id);
    if (typeof resp == 'undefined') {
      throw new HttpException('Not Found', HttpStatus.NOT_FOUND);
    }
    return resp;
  }

  @Patch(':id')
  @ApiResponse({status: 200, description: 'The matching todo was updated'})
  @ApiResponse({status: 404, description: 'There is no matching todo'})
  async update(@Param('id') id: string, @Body() updateTodoDto: UpdateTodoDto) {
    let resp = await this.todoService.update(+id, updateTodoDto);
    if (typeof resp == 'undefined') {
      throw new HttpException('Not Found', HttpStatus.NOT_FOUND);
    }
    return resp;
  }

  @Delete(':id')
  @ApiResponse({status: 200, description: 'The matching todo was deleted'})
  @ApiResponse({status: 404, description: 'There is no matching todo'})
  async remove(@Param('id') id: string) {
    // delete will always return an ok, even if missing
    return await this.todoService.remove(+id);
  }
}
