import { Injectable, Query } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Todo } from './entities/todo.entity';
import { CreateTodoDto } from './dto/create-todo.dto';
import { ListTodoDto } from './dto/list-todo.dto';
import { UpdateTodoDto } from './dto/update-todo.dto';

var todos = [];

@Injectable()
export class TodoService {
  constructor(
    @InjectRepository(Todo)
    private todoRepository: Repository<Todo>,
  ) {}

  async create(createTodoDto: CreateTodoDto) {
    let resp = await this.todoRepository.insert(createTodoDto);
    createTodoDto.id = resp.identifiers[0].id;
    return createTodoDto
  }

  async findAll(@Query() query: ListTodoDto) {
    let resp = await this.todoRepository.find({
      where: { status: query.status },
    });
    return resp;
  }

  async findOne(id: number) {
    let resp = await this.todoRepository.findOne(id);
    return resp;
  }

  async update(id: number, updateTodoDto: UpdateTodoDto) {
    let resp = await this.findOne(id);
    if (typeof resp == 'undefined') {
      return resp;
    }
    await this.todoRepository.update({ id: id }, updateTodoDto);
    return 'ok';
  }

  async remove(id: number) {
    await this.todoRepository.delete({ id: id });
    return 'ok';
  }
}
