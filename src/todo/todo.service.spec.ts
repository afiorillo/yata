import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Status } from './entities/status.entity';
import { TodoService } from './todo.service';
import { Todo } from './entities/todo.entity';
import { mockRepository, newTodo, todoArray } from './mock/todos.mock';
import exp from 'constants';

describe('TodoService', () => {
  let service: TodoService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        TodoService,
        {
          // credit where credit is due, this mocking is heavily inspired by
          // https://github.com/jmcdo29/testing-nestjs/blob/master/apps/typeorm-sample/src/cat/cat.service.spec.ts
          provide: getRepositoryToken(Todo),
          useValue: mockRepository,
        },
      ],
    }).compile();

    service = module.get<TodoService>(TodoService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should allow creation', async () => {
    // it returns the primary key
    let createdTodo = newTodo
    createdTodo.id = 1
    await expect(service.create(newTodo)).resolves.toEqual(createdTodo);
  });

  it('should allow reading by status', async () => {
    // returns a list of todos
    await expect(
      service.findAll({ status: Status.IN_PROGRESS }),
    ).resolves.toEqual(todoArray);
  });

  it('should allow reading by id', async () => {
    await expect(service.findOne(1)).resolves.toBeDefined();
  });

  it('should use undefined if ID is missing', async () => {
    mockRepository.findOne.mockReturnValueOnce(undefined);
    await expect(service.findOne(1)).resolves.toBeUndefined();
  });

  it('allows updating', async () => {
    await expect(service.update(1, { status: Status.DONE })).resolves.toEqual(
      'ok',
    );
  });

  it('allows deletion', async () => {
    await expect(service.remove(1)).resolves.toEqual('ok');
  });
});
