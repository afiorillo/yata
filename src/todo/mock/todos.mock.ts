import { Status } from '../entities/status.entity';
import { Todo } from '../entities/todo.entity';

export const todoArray = [
  new Todo('foo', Status.IN_PROGRESS),
  new Todo('bar', Status.DONE),
  new Todo('baz', Status.DONE),
];

export const newTodo = new Todo('boo', Status.IN_PROGRESS);

export const mockRepository = {
  insert: jest.fn().mockReturnValue({ identifiers: [{ id: 1 }] }),
  find: jest.fn().mockReturnValue(todoArray),
  findOne: jest.fn().mockReturnValue(newTodo),
  update: jest.fn().mockReturnValue(true),
  delete: jest.fn().mockReturnValue(true),
};
