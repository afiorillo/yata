import { IsEnum, IsNotEmpty } from 'class-validator';
import { Column, Entity, Index, PrimaryGeneratedColumn } from 'typeorm';
import { Status } from './status.entity';

@Entity()
export class Todo {
  @PrimaryGeneratedColumn()
  id: number;

  @IsNotEmpty()
  @Column('text')
  text: string;

  @IsEnum(Status)
  @Column('text')
  @Index("status-idx")
  status: Status;

  constructor(text: string, status: Status) {
    this.text = text;
    this.status = status;
  }
}
