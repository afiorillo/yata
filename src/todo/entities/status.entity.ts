
export enum Status {
  IN_PROGRESS = 'in_progress',
  DONE = 'done',
}
