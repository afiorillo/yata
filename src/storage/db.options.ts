import { readFile } from 'fs';

// need to read the secrets from secrets
let username;
if (typeof process.env.DB_USER != 'undefined') {
  username = process.env.DB_USER;
} else {
  readFile(process.env.DB_USER_FILE, 'utf8', (err, data) => {
    if (err) {
      return console.log(err);
    }
    username = data;
  });
}

let password;
if (typeof process.env.DB_PASSWORD != 'undefined') {
  password = process.env.DB_PASSWORD;
} else {
  readFile(process.env.DB_PASSWORD_FILE, 'utf8', (err, data) => {
    if (err) {
      return console.log(err);
    }
    password = data;
  });
}

export const DB_HOST = process.env.DB_HOST
export const DB_PORT = parseInt(process.env.DB_PORT, 10)
export const DB_USER = username
export const DB_PASSWORD = password