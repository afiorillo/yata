import { ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  const config = new DocumentBuilder()
    .setTitle('Yata')
    .setDescription('Yet Another Todo App')
    .setVersion('1.0')
    .build()
  const doc = SwaggerModule.createDocument(app, config)
  SwaggerModule.setup('docs', app, doc)

  app.useGlobalPipes(
    new ValidationPipe({
      // set to true so responses don't exfiltrate API behavior
      disableErrorMessages: false,
    }),
  );
  await app.listen(3000);
}
bootstrap();
