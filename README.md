# Yata: Yet Another Todo Application

## Description

This project is meant to be a model todo application, build on Nest.js (using Express.js and TypeORM under the hood).

## Installation

```bash
$ npm install
```

## Running the app

### Locally

Note that you must have a Postgres instance running before starting the server.
A quick way to do this is with Docker:

```bash
$ export DB_USER=mysecret DB_PASSWORD=even-more-secret!
$ docker run --rm -it -p 5432:5432 -e POSTGRES_USER=$DB_USER -e POSTGRES_PASSWORD=$DB_PASSWORD postgres:alpine
```

After starting postgres you can run the server with:

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

### Deployment

This project is easy to deploy with `docker-compose`.

First, you must put the database secrets somewhere secure.
E.g.

```bash
$ mkdir secrets
$ echo "myusername" > ./secrets/db_username.txt
$ echo "mypassword" > ./secrets/db_password.txt
$ chmod -R g-r secrets
```

And then a little `docker-compose up` should turn things on.

To clean up (including destroy the DB) run `docker-compose down -v`.

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```

## Design

To start, we can assign a common/simplified structure to Todos:

```ts
export interface Todo {
  // Todos need to have some descriptive text, e.g. "go grocery shopping"
  // In lieu of multiple fields for different purposes, we can use a simple
  // catch-all for this model.
  text: string;
  // Todos can go through multiple states, e.g. "in progress" and "done"
  status: string;
}
```

Our basic Todo application should have a few straightforward endpoints:

- a `GET /todos/?...` which returns a listing of todos that match the given query parameters
- a `POST /todo/` which creates a new entity
- a `GET /todo/:id` which reads that entity
- a `PATCH /todo/:id` which updates that entity
- a `DELETE /todo/:id` which deletes that entity
