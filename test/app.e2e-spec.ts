import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from './../src/app.module';

describe('AppController (e2e)', () => {
  let app: INestApplication;

  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  it('/ (GET)', () => {
    return request(app.getHttpServer())
      .get('/')
      .expect(302);
  });

  describe('CRUD flow', () => {
    let id = '1';

    it('allows creation', (done) => {
      request(app.getHttpServer())
        .post('/todo')
        .send({ text: 'foo', status: 'in_progress' })
        .expect(201)
        .then((resp) => {
          id = resp.body.id;
          done();
        })
        .catch((err) => done(err));
    });

    it('then allows getting by ID', (done) => {
      request(app.getHttpServer()).get(`/todo/${id}`).expect(
        200,
        done,
      );
    });

    // it('should allow updating by ID', (done) => {
    //   request(app.getHttpServer())
    //     .patch(`/todo/${id}`)
    //     .send({ status: 'done' })
    //     .expect(
    //       200,
    //       {
    //         text: 'foo',
    //         status: 'done',
    //       },
    //       done,
    //     );
    // });

    // it('should allow deletion by ID', (done) => {
    //   request(app.getHttpServer()).delete(`/todo/${id}`).expect(200, done);
    // });

    // it('should then no longer allow getting by ID', (done) => {
    //   request(app.getHttpServer()).get(`/todo/${id}`).expect(404, done);
    // });
  });
});
