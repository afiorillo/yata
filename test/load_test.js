import http from 'k6/http';
import { check, group } from 'k6';

export let options = {
  stages: [
    { duration: '30s', target: 20 },
    { duration: '60s', target: 10 },
    { duration: '20s', target: 0 },
  ],
};

const BASE_URL = 'http://127.0.0.1:3000';

const TODOS = [
  { text: 'grocery shopping', status: 'in_progress' },
  { text: 'write a book', status: 'in_progress' },
  { text: 'walk the dog', status: 'in_progress' },
];

export default function () {
  group('create some todos, then do them', (_) => {
    // create them

    let createdTodos = TODOS.map((todo) => {
      let resp = http.post(BASE_URL + '/todo/', todo);
      check(resp, {
        'is status 201': (r) => r.status === 201,
        'contains an id': (r) => typeof r.json() != 'undefined',
      });
      return resp.json();
    });

    // query them

    let resp = http.get(BASE_URL + '/todo/?status=in_progress');
    check(resp, {
      'query : 200': (r) => r.status === 200,
      'query : answer bigger than 3': (r) => r.json().length >= 3,
    });

    // update some
    let toUpdate = [0, 2];
    toUpdate.forEach((idx) => {
      let todo = TODOS[idx];
      let todoId = createdTodos[idx];
      todo.status = 'done';
      resp = http.patch(BASE_URL + '/todo/' + todoId, todo);
      check(resp, {
        // 'patch : 200': (r) => {r.status === 200},
        // 'patch : ok': (r) => {
        //   r.body === 'ok';
        // },
      });
    });

    // get one

    resp = http.get(BASE_URL + '/todo/' + createdTodos[1]);
    check(resp, {
      // 'get : 200': (r) => {
      //   r.status === 200;
      // },
    });

    // clean up

    createdTodos.forEach((id) => {
      resp = http.del(BASE_URL + '/todo/' + id);
      check(resp, {
        'delete : 200': (r) => r.status === 200,
        'delete : answer ok': (r) => r.body === 'ok',
      });
    });
  });
}
