#!/usr/bin/env bash

docker run --rm --network=host -it -v "$(pwd)/test:/home/k6/test:ro" loadimpact/k6 run test/load_test.js
